package com.yheriatovych.flkr;

import com.yheriatovych.flkr.data.AppState;
import com.yheriatovych.flkr.data.FlickrActions;
import com.yheriatovych.reductor.Action;
import com.yheriatovych.reductor.Actions;
import com.yheriatovych.reductor.Cancelable;
import com.yheriatovych.reductor.Cursors;
import com.yheriatovych.reductor.Store;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

public class FlickrPresenterImpl implements FlickrPresenter {

    private static final FlickrActions actionCreator = Actions.from(FlickrActions.class);

    private final Store<AppState> store;
    private final CompositeDisposable compositeDisposable;
    private final PublishSubject<String> querySubject = PublishSubject.create();

    @Inject
    FlickrPresenterImpl(Store<AppState> store) {
        this.store = store;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onGridEndReached() {
        if (!store.getState().loading()) {
            store.dispatch(actionCreator.loadMore());
        }
    }

    @Override
    public void queryChanged(String query) {
        querySubject.onNext(query);
    }

    @Override
    public void attachView(View view) {
        compositeDisposable.add(querySubject.debounce(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .distinctUntilChanged()
                .subscribe(query -> store.dispatch(actionCreator.search(query))));

        Cancelable cancelable = Cursors.forEach(store, appState -> {
            view.setItems(appState.images());
            AppState.Error error = appState.error();
            if (error != null) {
                Runnable retryCallback = null;
                Action retryAction = error.retryAction();
                if (retryAction != null) {
                    retryCallback = () -> store.dispatch(retryAction);
                }
                view.showLoadingError(retryCallback);
                store.dispatch(actionCreator.errorHandled());
            }
        });
        compositeDisposable.add(Disposables.fromAction(cancelable::cancel));
    }

    @Override
    public void detachView() {
        compositeDisposable.clear();
    }
}
