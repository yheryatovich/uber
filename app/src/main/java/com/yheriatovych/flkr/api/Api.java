package com.yheriatovych.flkr.api;

import com.yheriatovych.flkr.model.FlkrResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by yaroslavheriatovych on 23/06/2017.
 */

public interface Api {
    @GET("services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1")
    Single<FlkrResponse> search(@Query("text") String query, @Query("page") int page);
}
