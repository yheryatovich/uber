package com.yheriatovych.flkr;

import android.support.annotation.Nullable;

import com.github.andrewoma.dexx.collection.List;
import com.yheriatovych.flkr.model.FlickrImage;

/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

public interface FlickrPresenter {
    void onGridEndReached();

    void queryChanged(String query);

    interface View {
        void setItems(List<FlickrImage> images);

        void showLoadingError(@Nullable Runnable retryCallback);
    }




    void attachView(View view);
    void detachView();
}
