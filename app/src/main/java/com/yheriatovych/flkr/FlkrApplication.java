package com.yheriatovych.flkr;

import android.app.Application;
import android.content.Context;

import com.yheriatovych.flkr.dagger.DaggerFlkrComponent;
import com.yheriatovych.flkr.dagger.FlkrComponent;

/**
 * Created by yaroslavheriatovych on 23/06/2017.
 */

public class FlkrApplication extends Application {
    private FlkrComponent component;
    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerFlkrComponent.create();
    }

    public static FlkrApplication getApp(Context context) {
        return (FlkrApplication) context.getApplicationContext();
    }

    public FlkrComponent getComponent() {
        return component;
    }
}
