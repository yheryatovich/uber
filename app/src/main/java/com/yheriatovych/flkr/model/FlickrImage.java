package com.yheriatovych.flkr.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.auto.value.AutoValue;
import com.google.auto.value.extension.memoized.Memoized;

/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

@AutoValue
public abstract class FlickrImage {

    public abstract String id();

    public abstract String server();

    public abstract int farm();

    public abstract String secret();

    @JsonCreator
    public static FlickrImage create(
            @JsonProperty("id") String id,
            @JsonProperty("server") String server,
            @JsonProperty("farm") int farm,
            @JsonProperty("secret") String secret
    ) {
        return new AutoValue_FlickrImage(id, server, farm, secret);
    }

    @Memoized
    public String getUrl() {
        return "http://farm" + farm() + ".static.flickr.com/" + server() + "/" + id() + "_" + secret() + ".jpg";
    }
}
