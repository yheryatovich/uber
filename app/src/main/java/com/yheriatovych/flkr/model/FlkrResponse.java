package com.yheriatovych.flkr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by yaroslavheriatovych on 23/06/2017.
 */

public class FlkrResponse {
    @JsonProperty("stat")
    public String status;

    @JsonProperty("photos")
    public PhotosContent photosContent;


    public static class PhotosContent {
        @JsonProperty("page")
        public int page;

        @JsonProperty("photo")
        public List<FlickrImage> photos;
    }
}
