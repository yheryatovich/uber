package com.yheriatovych.flkr.data;

import android.support.annotation.Nullable;

import com.github.andrewoma.dexx.collection.List;
import com.google.auto.value.AutoValue;
import com.yheriatovych.flkr.model.FlickrImage;
import com.yheriatovych.reductor.Action;


/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

@AutoValue
public abstract class AppState {
    public abstract String query();
    public abstract List<FlickrImage> images();
    public abstract boolean loading();
    @Nullable
    public abstract Error error();
    public abstract int page();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_AppState.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder images(List<FlickrImage> images);

        public abstract Builder loading(boolean loading);

        public abstract Builder query(String query);

        public abstract Builder page(int page);

        public abstract Builder error(Error error);

        public abstract AppState build();
    }

    @AutoValue
    public static abstract class Error {
        public abstract Throwable throwable();
        @Nullable
        public abstract Action retryAction();

        public static Error create(Throwable throwable, Action retryAction) {
            return new AutoValue_AppState_Error(throwable, retryAction);
        }
    }
}
