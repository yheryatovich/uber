package com.yheriatovych.flkr.data;

import com.github.andrewoma.dexx.collection.IndexedLists;
import com.github.andrewoma.dexx.collection.List;
import com.yheriatovych.flkr.model.FlickrImage;
import com.yheriatovych.reductor.Reducer;
import com.yheriatovych.reductor.annotations.AutoReducer;

/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

@AutoReducer
public abstract class AppStateReducer implements Reducer<AppState> {

    @AutoReducer.InitialState
    static AppState initialState(){
        return AppState.builder()
                .images(IndexedLists.of())
                .loading(false)
                .query("")
                .page(0)
                .build();
    }

    @AutoReducer.Action(value = FlickrActions.SEARCH, from = FlickrActions.class)
    AppState search(AppState state, String query) {
        if(state.query().equals(query)) return state;

        return state.toBuilder()
                .images(IndexedLists.of())
                .query(query)
                .loading(true)
                .build();
    }

    @AutoReducer.Action(value = FlickrActions.LOAD_NEXT_CHUNK, from = FlickrActions.class)
    AppState loadMore(AppState state) {
        if(state.loading()) return state;
        return state.toBuilder()
                .loading(true)
                .build();
    }

    @AutoReducer.Action(value = FlickrActions.CHUNK_LOADED, from = FlickrActions.class)
    AppState chunkLoaded(AppState state, boolean initialChunk, java.util.List<FlickrImage> images, int page) {
        List<FlickrImage> items;
        if(initialChunk) {
            items = IndexedLists.copyOf(images);
        } else {
            items = state.images();
            for (FlickrImage image : images) {
                items = items.append(image);
            }
        }
        return state.toBuilder()
                .images(items)
                .page(page)
                .loading(false)
                .build();
    }

    @AutoReducer.Action(value = FlickrActions.LOADING_FAILED, from = FlickrActions.class)
    AppState loadingFailed(AppState state, AppState.Error error) {
        return state.toBuilder()
                .loading(false)
                .error(error)
                .build();
    }

    @AutoReducer.Action(value = FlickrActions.ERROR_HANDLED, from = FlickrActions.class)
    AppState errorHandled(AppState state) {
        return state.toBuilder()
                .error(null)
                .build();
    }

    public static AppStateReducer create() {
        return new AppStateReducerImpl();
    }
}
