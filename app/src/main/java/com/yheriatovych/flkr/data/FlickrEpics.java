package com.yheriatovych.flkr.data;

import com.yheriatovych.flkr.api.Api;
import com.yheriatovych.reductor.Actions;
import com.yheriatovych.reductor.observable.rxjava2.Epic;
import com.yheriatovych.reductor.observable.rxjava2.Epics;

import java.util.Arrays;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

public class FlickrEpics {
    private static final FlickrActions actionCreator = Actions.from(FlickrActions.class);

    private static Epic<AppState> search(Api api) {
        return (actions, store) -> actions.filter(Epics.ofType(FlickrActions.SEARCH))
                .switchMapSingle(action -> {
                    String query = action.getValue(0);
                    return api.search(query, 1)
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(flkrResponse -> actionCreator.imagesLoaded(true, flkrResponse.photosContent.photos, flkrResponse.photosContent.page))
                            .onErrorReturn(ex -> actionCreator.loadingFailed(AppState.Error.create(ex, action)));
                });

    }

    private static Epic<AppState> loadMore(Api api) {
        return (actions, store) -> actions.filter(Epics.ofType(FlickrActions.LOAD_NEXT_CHUNK))
                .switchMapSingle(action -> {
                    AppState state = store.getState();
                    String query = state.query();
                    int page = state.page();
                    return api.search(query, page + 1)
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(flkrResponse -> actionCreator.imagesLoaded(false, flkrResponse.photosContent.photos, flkrResponse.photosContent.page))
                            .onErrorReturn(ex -> actionCreator.loadingFailed(AppState.Error.create(ex, action)));
                });

    }


    public static Epic<AppState> allEpics(Api api) {
        return Epics.combineEpics(Arrays.asList(
                search(api),
                loadMore(api)
        ));
    }
}
