package com.yheriatovych.flkr.data;

import com.yheriatovych.flkr.model.FlickrImage;
import com.yheriatovych.reductor.Action;
import com.yheriatovych.reductor.annotations.ActionCreator;

import java.util.List;

/**
 * Created by yaroslavheriatovych on 24/06/2017.
 */

@ActionCreator
public interface FlickrActions {
    String SEARCH = "SEARCH";
    String LOAD_NEXT_CHUNK = "LOAD_NEXT_CHUNK";
    String CHUNK_LOADED = "CHUNK_LOADED";
    String LOADING_FAILED = "LOADING_FAILED";
    String ERROR_HANDLED = "ERROR_HANDLED";

    @ActionCreator.Action(SEARCH)
    Action search(String query);

    @ActionCreator.Action(LOAD_NEXT_CHUNK)
    Action loadMore();

    @ActionCreator.Action(CHUNK_LOADED)
    Action imagesLoaded(boolean initialChunk, List<FlickrImage> photos, int page);

    @ActionCreator.Action(LOADING_FAILED)
    Action loadingFailed(AppState.Error error);

    @ActionCreator.Action(ERROR_HANDLED)
    Action errorHandled();
}
