package com.yheriatovych.flkr;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.andrewoma.dexx.collection.IndexedLists;
import com.github.andrewoma.dexx.collection.List;
import com.yheriatovych.flkr.model.FlickrImage;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements FlickrPresenter.View {

    @BindView(R.id.grid)
    RecyclerView recyclerView;

    @BindView(R.id.search)
    EditText searchEditText;

    @Inject
    FlickrPresenter presenter;
    private PhotosAdapter adapter;

    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FlkrApplication.getApp(this).getComponent().inject(this);

        GridLayoutManager layout = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layout);
        adapter = new PhotosAdapter(this);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (layout.findLastVisibleItemPosition() > adapter.getItemCount() - 6) {
                    handler.post(() -> presenter.onGridEndReached());
                }
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.queryChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    @Override
    public void setItems(List<FlickrImage> images) {
        adapter.setImages(images);
    }

    @Override
    public void showLoadingError(@Nullable Runnable retryCallback) {
        Snackbar snackbar = Snackbar.make(recyclerView, R.string.loading_error, Snackbar.LENGTH_SHORT);
        if(retryCallback != null) {
            snackbar.setAction(R.string.retry, v -> retryCallback.run());
        }
        snackbar.show();
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private static class PhotosAdapter extends RecyclerView.Adapter<PhotoViewHolder> {

        private List<FlickrImage> images = IndexedLists.of();
        private final Activity activity;

        PhotosAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new PhotoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false));
        }

        @Override
        public void onBindViewHolder(PhotoViewHolder holder, int position) {
            FlickrImage flickrImage = images.get(position);
            Glide.with(activity)
                    .load(flickrImage.getUrl())
                    .apply(RequestOptions.placeholderOf(android.R.drawable.star_on))
                    .into(holder.image);
        }

        @Override
        public int getItemCount() {
            return images.size();
        }

        void setImages(List<FlickrImage> images) {
            this.images = images;
            notifyDataSetChanged();
        }
    }
}
