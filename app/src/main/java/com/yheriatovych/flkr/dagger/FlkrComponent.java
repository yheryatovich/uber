package com.yheriatovych.flkr.dagger;

import com.yheriatovych.flkr.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by yaroslavheriatovych on 23/06/2017.
 */

@Component(modules = FlickerModule.class)
@Singleton
public interface FlkrComponent {
    void inject(MainActivity mainActivity);
}
