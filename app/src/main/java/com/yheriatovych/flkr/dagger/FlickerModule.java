package com.yheriatovych.flkr.dagger;

import android.util.Log;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yheriatovych.flkr.FlickrPresenter;
import com.yheriatovych.flkr.FlickrPresenterImpl;
import com.yheriatovych.flkr.api.Api;
import com.yheriatovych.flkr.data.AppState;
import com.yheriatovych.flkr.data.AppStateReducer;
import com.yheriatovych.flkr.data.FlickrEpics;
import com.yheriatovych.reductor.Middleware;
import com.yheriatovych.reductor.Store;
import com.yheriatovych.reductor.observable.rxjava2.EpicMiddleware;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by yaroslavheriatovych on 23/06/2017.
 */

@Module
public class FlickerModule {
    @Provides
    @Singleton
    OkHttpClient provideHttpClient() {
        return new OkHttpClient.Builder()
                .build();
    }

    @Provides
    @Singleton
    ObjectMapper provideObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper;
    }

    @Provides
    Api provideApi(OkHttpClient client, ObjectMapper objectMapper) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.flickr.com/")
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .client(client)
                .build();

        return retrofit.create(Api.class);
    }

    @Provides
    @Singleton
    Store<AppState> provideStore(Api api) {
        EpicMiddleware<AppState> epicMiddleware = EpicMiddleware.create(FlickrEpics.allEpics(api));
        Middleware<AppState> logging = (store, dispatcher) -> o -> {
            Log.d("ACTION", "" + o);
            dispatcher.dispatch(o);
        };
        return Store.create(AppStateReducer.create(), epicMiddleware, logging);
    }

    @Provides
    FlickrPresenter providePresenter(FlickrPresenterImpl impl) {
        return impl;
    }
}
